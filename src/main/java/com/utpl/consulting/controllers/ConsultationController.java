package com.utpl.consulting.controllers;

import com.utpl.consulting.models.entity.Consultation;
import com.utpl.consulting.models.services.ConsultationServiceImplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/consultations")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
public class ConsultationController {

    @Autowired
    private ConsultationServiceImplement consultationSImplement;

    @GetMapping
    public List<Consultation> ListConsultations(){
        return consultationSImplement.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Consultation addConsultation(@RequestBody Consultation consultation){
        return consultationSImplement.save(consultation);
    }

}
