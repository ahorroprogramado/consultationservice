package com.utpl.consulting.controllers;

import com.utpl.consulting.models.dao.ConsultationDao;
import com.utpl.consulting.models.entity.Consultation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;

@Controller
public class CosultationViewController {
   
    @Autowired
    ConsultationDao consultationDao;

    @GetMapping(value = "/")
    public String index(Model model){
        // model.addAttribute("consultation", new Consultation());
        model.addAttribute("consultations",consultationDao.findAll());
        return "index";
    }

    @GetMapping(value="/add/{id}")
    public String showAdd(@PathVariable("id") Long id, Model model){
        if(id != null && id != 0){
            model.addAttribute("consultation", consultationDao.findAllById(Collections.singleton(id)));
        }else{
            model.addAttribute("consultation", new Consultation());
        }
        return "add";
    }

    @PostMapping(value="/add")
    public String addConsultation(Consultation consultation, Model model){
        consultationDao.save(consultation);
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id, Model model){
        consultationDao.deleteById(id);
        return "redirect:/";
    }
}
