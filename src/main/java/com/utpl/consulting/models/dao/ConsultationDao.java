package com.utpl.consulting.models.dao;

import com.utpl.consulting.models.entity.Consultation;
import org.springframework.data.repository.CrudRepository;

public interface ConsultationDao extends CrudRepository<Consultation, Long> {
    

}
