package com.utpl.consulting.models.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_consulting")
public class Consultation {

    @Id
    @Column(name = "id_consult")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    public String account_name;
    public String description;
    public boolean status;

    @Column(name = "application_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date application_date;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        if(account_name.isBlank()) this.account_name = "PAUL RAMON";
        else this.account_name = account_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus() {
        this.status = false;
    }

    public Date getApplication_date() {
        return application_date;
    }

    public void setApplication_date(Date application_date) {
        this.application_date = application_date;
    }

}
