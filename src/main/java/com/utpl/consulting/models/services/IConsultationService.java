package com.utpl.consulting.models.services;

import com.utpl.consulting.models.entity.Consultation;

import java.util.List;

public interface IConsultationService {
    public List<Consultation> findAll();
    public Consultation save(Consultation consultation);

}
