package com.utpl.consulting.models.services;

import com.utpl.consulting.models.dao.ConsultationDao;
import com.utpl.consulting.models.entity.Consultation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ConsultationServiceImplement implements IConsultationService{

    @Autowired
    private ConsultationDao consultationDao;

    @Override
    @Transactional(readOnly = true)
    public List<Consultation> findAll() {
        return (List<Consultation>) consultationDao.findAll();
    }

    @Override
    public Consultation save(Consultation consultation) {
        return consultationDao.save(consultation);
    }
}
