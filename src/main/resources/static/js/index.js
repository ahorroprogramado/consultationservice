document.getElementById('application_date').value = getDate();
enableButton()
function getDate(){
    let date = new Date()
    let day = date.getDate()
    let month = date.getMonth() + 1
    let year = date.getFullYear()
    console.log(date)
    let dateActual;
    if(month < 10 && day < 10){
      return`${year}-0${month}-0${day}`
    }else if(month < 10 && day >= 10){
      return `${year}-0${month}-${day}`
    }else{
      return `${year}-${month}-${day}`
    }
}

function enableButton(){
    let btn  = document.getElementById("send")
    let desc = document.getElementById('description')
    desc.addEventListener("keyup", () => {
              if(desc.value != "" && desc.value != " " && desc.value != "  " && desc.value != "  ") {
                  document.getElementById("send").disabled = false
              } else {
                  document.getElementById("send").disabled = true
              }
            })
    /*btn.addEventListener("click", (event) => {
                  event.preventDefault()
            })*/
        //document.getElementById("send").disabled = false;
}